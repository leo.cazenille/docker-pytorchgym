FROM nvidia/cuda:10.1-base-ubuntu18.04
MAINTAINER leo.cazenille@gmail.com

ENV DEBIAN_FRONTEND noninteractive

# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    git \
    bzip2 \
    libx11-6 \
    python3-yaml \
    gosu \
    rsync \
    python3-opengl \
    python3-dev \
    python3-pip \
    build-essential \
    cmake \
    swig \
 && rm -rf /var/lib/apt/lists/*

# Create a working directory
RUN mkdir /home/user
WORKDIR /home/user

## Create a non-root user and switch to it
#RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
# && chown -R user:user /home/user
#RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
#USER user

## All users can use /home/user as their home directory
#ENV HOME=/home/user
#RUN chmod 777 /home/user
#RUN mkdir /home/user

# Install MinicondA
RUN curl -so /home/user/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh \
 && chmod +x /home/user/miniconda.sh \
 && /home/user/miniconda.sh -b -p /home/user/miniconda \
 && rm /home/user/miniconda.sh
ENV PATH=/home/user/miniconda/bin:$PATH
ENV CONDA_AUTO_UPDATE_CONDA=false

# Create a Python 3.6 environment
RUN /home/user/miniconda/bin/conda create -y --name py36 python=3.6.9 \
 && /home/user/miniconda/bin/conda clean -ya
ENV CONDA_DEFAULT_ENV=py36
ENV CONDA_PREFIX=/home/user/miniconda/envs/$CONDA_DEFAULT_ENV
ENV PATH=$CONDA_PREFIX/bin:$PATH
RUN /home/user/miniconda/bin/conda install conda-build=3.18.9=py36_3 \
 && /home/user/miniconda/bin/conda clean -ya

# CUDA 10.1-specific steps
RUN conda install -y -c pytorch \
    cudatoolkit=10.1 \
    "pytorch=1.4.0=py3.6_cuda10.1.243_cudnn7.6.3_0" \
    "torchvision=0.5.0=py36_cu101" \
 && conda clean -ya

# Install HDF5 Python bindings
#RUN conda install -y h5py=2.8.0 \
# && conda clean -ya
#RUN pip install h5py-cache==1.0

# Install Torchnet, a high-level framework for PyTorch
#RUN pip install torchnet==0.0.4


RUN pip install gym==0.9.4 box2d-py PyOpenGL setproctitle pybullet qdpy[all] cma

#RUN pip3 --no-cache-dir install Cython
#RUN pip --no-cache-dir install Cython



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
